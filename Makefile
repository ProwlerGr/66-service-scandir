#
# This Makefile requires GNU make.
#
# Do not make changes here.
# Use the included .mak files.
#

make_need := 3.81
ifeq "" "$(strip $(filter $(make_need), $(firstword $(sort $(make_need) $(MAKE_VERSION)))))"
fail := $(error Your make ($(MAKE_VERSION)) is too old. You need $(make_need) or newer)
endif

-include config.mak
include package/targets.mak

INSTALL := ./tools/install.sh

install: install-module install-script
install-module: $(MODULE_TARGET:module/%=$(DESTDIR)$(service_directory)/$(package)/%)
install-script: $(SCRIPT_TARGET:module/configure/%=$(DESTDIR)$(service_directory)/$(package)/configure/%)

$(DESTDIR)$(service_directory)/$(package)/%: module/%
	exec $(INSTALL) -D -m 644 $< $@
	sed -i -e 's,@VERSION@,$(version),' \
		-e "s,@LIVEDIR@,$(livedir)," \
		-e "s,@USER_CONF_DIR@,$(service_userconf)," \
		-e "s,@LOGGER@,$(LOGGER)," \
		-e "s,@USE_ENVIRONMENT@,$(USE_ENVIRONMENT)," \
		-e "s,@VERBOSITY_LEVEL@,$(VERBOSITY_LEVEL)," \
		-e "s,@NOTIFY@,$(NOTIFY)," $@

$(DESTDIR)$(service_directory)/$(package)/configure/%: module/configure/%
	exec $(INSTALL) -D -m 755 $< $@
	sed -i -e 's,@BINDIR@,$(bindir),' $@

version:
	@echo $(version)

.PHONY: install version

.DELETE_ON_ERROR:
