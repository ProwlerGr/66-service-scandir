# Changelog for scandir-66serv

---

# In 0.4.2

- Adapt to 66 version 0.7.0 stable

- Allow to declare version at configure invocation

---

# In 0.4.1

- Adapt to 66 0.7.0.0-beta2

- Fix replacement of variable value

---

# In 0.4.0

- Adapt to execline 2.9.4.0

- Adapt to 66 0.7.0.0-beta

- Adapt to 66-tools 0.1.0.0

---

# In 0.3.1

- Follow 66 envfile behavior: the $HOME/.66/svscan@<user>/.svscan@<user> is now created with a dot.

---

# In 0.3.0

- Adapt to execline 2.7.0.0

- Adapt to 66 0.6.0.0

---

# In 0.2.1

- Convenient release to udpate dependencies:
    - 66 v0.5.1.0
    - 66-tools v0.0.6.2
    - optional dependency:
        - dbus-66serv 0.2.0 for environment configuration

---

# In 0.2.0

- Adapt to 66 v0.4.0.1

---

# In 0.1.0

- first commit
